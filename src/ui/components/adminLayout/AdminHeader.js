import React, {Component} from 'react';
import { render } from 'react-dom';

import MetaTags from 'react-meta-tags';
import $ from "jquery"
import swiper from "swiper";
import tooltip from "react-bootstrap";
/*import {loginContainer} from '../../common/containers/login'
import {logout} from '../../common/containers/logout'*/


class AdminHeaderContent extends Component{
  componentDidMount(){

    $( document ).ready(function() {
      $(".search-submit").click(function(){
      $(this).parent().parent().toggleClass("search-open");
      });
    });



    /*$('#query').typeahead({
        local: ['India','Afghanistan','Australia','Germani','France','Hong Kong','USA','England','Africa','Argentina']
    });*/

    var swiper = new Swiper('.breadcrumb-slider', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView:2,
        centeredSlides: true,
        spaceBetween:5,
    });

    var mySwiper = new Swiper('.swiper-menu', {
        speed: 400,
        spaceBetween: 100,
        slidesPerView:'auto',
        spaceBetween:30,
    });

//$('[data-toggle="tooltip"]').tooltip();

        $('.search-btn').click(function () {
      $(".hcm-search-form").toggleClass("open");
    });

    $('.display-profile figure .user-menu').click(function(){
      $(this).toggleClass('rotation');
      $(this).parent().parent().find('.profile-data').slideToggle();

    })

    //side nav js
        $(".cross").hide();
        $(".menu").hide();
        $(".hamburger").click(function () {
            $(".menu").animate({
                height: 'toggle'
            }, 500, function () {
                $(".hamburger").hide();
                $(".cross").show();
            });
        });
        $(".cross").click(function () {
            $(".menu").animate({
                height: 'toggle'
            }, 500, function () {
                $(".cross").hide();
                $(".hamburger").show();
            });
        });

        //Action-button js
        $('.hcm-action-btn .action-btn').click(function(){
            $(this).parent().find('.hcm-action-elements').toggleClass('open');
        });

        //top_navigation
        $('.navigation ul li a').click(function(){
          $(this).parent().addClass('active');
          $(this).parent().siblings().removeClass('active');
        });

      var mySwiper = new Swiper('.navigation-swiper',{
        speed: 400,
        slidesPerView:'auto',
        spaceBetween:50,
      });


  }
   getRoutePath(){
      /* return FlowRouter.getRouteName()?FlowRouter.getRouteName():"";*/
    }
  render(){
     const routeName=this.getRoutePath();
     const isDashBoardRoute=routeName&&routeName.indexOf('dashboard')>=0?true:false;
     const isrmDetailsRoute=routeName&&routeName.indexOf('rmDetails')>=0?true:false;
     const isDocumentsRoute=routeName&&routeName.indexOf('documents')>=0?true:false;
     const isTransactionRequestedDetailsRoute=routeName&&routeName.indexOf('trDetails')>=0?true:false;
     const isSettingsRoute=routeName&&routeName.indexOf('pasettings')>=0?true:false;
    const isSettingsGeneralRoute=routeName&&routeName.indexOf('paSettingsGen')>=0?true:false;
    const isSettingsDocsRoute=routeName&&routeName.indexOf('paSettingsDocs')>=0?true:false;



    return (
      <div>
          <MetaTags>
              <title>HCM</title>
              <meta name="viewport" content="width=device-width, initial-scale=1" />
           </MetaTags>
          <div className="main-header">
              <div className="header">



              <div className="header-left pull-left">
               <div className="client-logo">
               <img src="/images/client-cog.png" />
               </div>
               <div className="menu-trigger">
               <div className="menu-icon-support">
                <span className="hamburger"><img src="/images/menu-icon.png" /></span>
                <span className="cross"><img src="/images/menu-icon.png" /></span>
                </div>
               </div>

               <div className="vTimeline"><ul><li>Home</li><li className="current">Dashboard</li><li>Management</li><li>People</li><li className="timelineLast"></li></ul></div>

              </div>
              <div className="header-mid"> <a href="">
               <img src="/images/hcm-logo.png" /></a>
              </div>
              <div className="header-right  pull-right">
                <div className="hcm-search-menu">
                  <div className="search-btn">
                      <img src="/images/search-icon.png" alt="search" />
                  </div>
                  <div className="hcm-search-form">
                    <input type="text" className="form-control" id="usr" />
                  </div>
                </div>
                <div className="hcm-profile-menu">
                  <div className="hcm-notification pull-right">
                    <span><span className="notification-alert">5</span><img src="/images/alert-image.png" className="padt15"/></span>
                  </div>
                  <div className="display-profile pull-right">
                      <figure>
                        <img src="/images/user.jpg"/>
                        <div className="user-menu">
                          <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
                        </div>
                        </figure>
                      <label>Hi, Usha</label>

                      <div className="profile-data">
                        <ul>
                          <li><span><i className="fa fa-tachometer" aria-hidden="true"></i></span>  <a href="#"> Settings</a></li>
                          <li><span><i className="fa fa-users" aria-hidden="true"></i></span>  <a href="W"> Switch</a></li>
                          <li><span><i className="fa fa-money" aria-hidden="true"></i></span>  <a href="#"> Payroll</a></li>
                          <li><span><i className="fa fa-line-chart" aria-hidden="true"></i></span>  <a href="#"> Performance</a></li>
                        </ul>
                      </div>
                  </div>
                  <div className="clearfix"></div>
                </div>
         </div>


              </div>

              <div className="navigation-wrap">

              {/*{isSettingsRoute && (*/}
                  {/*<div className="navigation swiper-container swiper-menu navigation-swiper">*/}
                      {/*<ul className="swiper-wrapper">*/}
                          {/*<li className="swiper-slide active"><a href="platformSettings">Address Type</a></li>*/}
                          {/*<li className="swiper-slide "><a href="settingsEmail">Email Type</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsContact">Contact number Type</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsGender">Gender</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsTitle">Title</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsCompany">Company Type</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsTax">Tax Type</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsZone">Zone Type</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsEntity">Entity</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsStage">Stage of Company</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsDomain">Domain</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsSubDomain">SubDomain</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsBusiness">Business Type</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsUser">User Type</a></li>*/}
                          {/*<li className="swiper-slide"><a href="settingsHistory">History</a></li>*/}
                      {/*</ul>*/}
                      {/*<div className="clearfix"></div>*/}
                  {/*</div>*/}
              {/*)*/}
              {/*}*/}
                  <div className="navigation swiper-container swiper-menu navigation-swiper">
                      <ul className="swiper-wrapper">
                          <li className="swiper-slide active"><a href="/admin/rmDetails">Details</a></li>
                          <li className="swiper-slide"><a href="/admin/rmUsers">Users</a></li>
                          <li className="swiper-slide"><a href="/admin/rmLibrary">Library</a></li>
                          <li className="swiper-slide"><a href="#">Transactions</a></li>
                          <li className="swiper-slide"><a href="#">History</a></li>
                      </ul>
                      <div className="clearfix"></div>
                  </div>
              </div>
          </div>
      </div>
  )
  }
}

export default AdminHeaderContent;
