import React, {Component} from 'react';
import MetaTags from 'react-meta-tags';
import ReactDOM from 'react-dom';
import logo from '../../../logo.svg';
import {Link} from 'react-router-dom';
import AdminHeader from './AdminHeader';
import Bootstrap from "bootstrap/dist/css/bootstrap.css";
import 'bootstrap/dist/js/bootstrap.min';
import AdminLeftNavigation from './AdminLeftNavigation';
import AdminFooterContent from './AdminFooter';
import settingsEntity from '../settingsEntity/SettingsEntity';

import "../stylesheets/css/header.scss";
import "../stylesheets/css/navigation.scss";
import "../stylesheets/css/footer.scss";
import "../stylesheets/css/reset.scss";
import '../stylesheets/css/hcm-admin.scss';
import '../stylesheets/css/components.scss';
import '../stylesheets/css/fonts.css';
class AdminLayout extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <div className="hcm-admin">
                    <MetaTags>
                        <title>hcmSprint</title>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    </MetaTags>
                    <AdminHeader/>
                    <AdminLeftNavigation/>
                    <div className="admin-main-wrap">
                        {this.props.children}
                    </div>
                    <AdminFooterContent/>
                </div>
            </div>
        );
    }
}

export default AdminLayout;