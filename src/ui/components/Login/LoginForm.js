import React, { PropTypes } from 'react';
import { Link, Redirect } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Card, CardText, CardActions, CardHeader} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const LoginForm = ({onSubmit, onChange, errors, user,redirect}) => (
    <div>
        <MuiThemeProvider>
            <Card className="container" style = {{"width" : "350px" , "height":"auto" , "margin" : "150px auto"}}>
            { redirect ? <Redirect to="/tasksGrid" />:
            ( <form onSubmit={onSubmit}>
                <CardHeader className="card-heading" style={{ "width":"100%","text-align":"center"}} title="Login"/>
                    {errors.summary && <p className="error-message">{errors.summary}</p>}
                <CardText>
                    <div className="field-line">
                        <TextField
                            floatingLabelText="Email"
                            name="email"
                            errorText={errors.email}
                            onChange={onChange}
                        />
                    </div>
                    <div className="field-line">
                        <TextField
                            floatingLabelText="Password"
                            type="password"
                            name="password"
                            onChange={onChange}
                            errorText={errors.password}
                        />
                    </div>
                    </CardText>
                <CardActions>
                    <div className="button-line">
                        <RaisedButton type="submit" label="Log in" primary />
                    </div>

                    <CardText>Don't have an account? <Link to={'/register'}>Create one</Link>.</CardText>
                    </CardActions>
                </form> )
                }
            </Card>
        </MuiThemeProvider>
    </div>
);

LoginForm.PropTypes = {
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.func.isRequired,
    user:PropTypes.func.isRequired
};

export default LoginForm;