/**
 * Created by anakha on 6/14/17.
 **/
import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import LoginForm from './LoginForm';
/*import LOGIN_QUERY from './LoginQuery.graphql';*/
import {graphql,gql} from 'react-apollo';


class LoginPage extends Component{
    constructor(props){
        super(props)
        this.state = {
            errors: '',
            user: {},
            redirect: false

        }
        this.OnSubmit = this.OnSubmit.bind(this);
        this.OnChange = this.OnChange.bind(this);

    }


    OnSubmit(event){
        event.preventDefault();
        //debugger;
        console.log("Hi submit");
        console.log(this.state.user);
        let user = this.state.user;
        let check = {};
        let email = this.state.user.email;
        let password = this.state.user.password;
        this.props.loginUser(email,password)
             .then((data) => {
                if(data.error){
                    check.success = false;
                    check.message = data.error.message;
                }else{
                    localStorage.setItem('token',data.data.loginUser.token);
                    check.success = true;
                    check.message ="200 OK";
                    this.setState({redirect: true});
                }
             })
             .catch(err => {
                alert(err.graphQLErrors[0].message.message);
                this.setState({errors: err})
             });

    }

    OnChange(event){
        console.log("Hi change");
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;
        console.log(user);
        this.setState({
            user
        });
    }
    render(){
        return(
            <LoginForm onSubmit={this.OnSubmit} onChange={this.OnChange} errors={this.state.errors} user={this.state.user} redirect={this.state.redirect}/>
        );
    }
}



const LOGIN_QUERY = gql`mutation loginUser($email: String!, $password: String!) {
  loginUser(email: $email, password: $password) {
    userId
    token
  }
}`;

const userAuthorization = graphql(LOGIN_QUERY,{
    props:({mutate}) => ({
        loginUser: (email,password) =>
            mutate({
                variables:{email,password}
            })
    })
})(LoginPage);


export default userAuthorization;
