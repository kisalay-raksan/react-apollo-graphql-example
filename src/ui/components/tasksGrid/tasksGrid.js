import React,{Component} from "react";
import RsTabular from "../rsTabular/rsTabular.js";
import { graphql,gql } from 'react-apollo';
class tasksGrid extends Component{
   render(){
       return(
           <div className="admin-padding-wrap">
               <div className="settings-container">
                    <RsTabular tableData={this.props.data.multipleTodos} tableName="tasks" />
                </div>
           </div>
       )
   }
}
const FeedQuery = gql`query multipleTodos {
    multipleTodos {
        _id
        taskName
        Author{
            _id
            email
        }
    }
}`;
export default graphql(FeedQuery)(tasksGrid);