/**
 * Created by anakha on 6/19/17.
 */
import React,{Component} from 'react';
import { render } from 'react-dom';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

var entityType = [{
    sno: 1,
    name: 'Private Limited',
    displayname:'Pvt Ltd.',
    status:'Active',
    createdby:'Varun',
    createddate:'16-02-16 04:15'
},
    {
        sno: 2,
        name: 'Public Limited',
        displayname:'Public Limited',
        status:'Active',
        createdby:'Arjun Singh',
        createddate:'23-02-16 10:23'
    },
    {
        sno: 3,
        name: 'Society',
        displayname:'Society',
        status:'Active',
        createdby:'Sachin',
        createddate:'22-02-16 01:25'
    },
    {
        sno: 4,
        name: 'Section & Company',
        displayname:'Section & Company',
        status:'Active',
        createdby:'Arjun Singh',
        createddate:'03-02-16 11:30'
    },
];
const selectRow = {
    mode: 'checkbox',
    bgColor: '#d4e0f5'
};
export default class SettingsEntity extends Component{
    componentDidMount()
    {
        $('.hcm-action-btn .action-btn').click(function(){
            $(this).parent().find('.hcm-action-elements').toggleClass('open');
        });
    }
    render(){
        debugger;
        return (
            <div className="admin-main-wrap">
                <div className="admin-padding-wrap no-padding">
                    <div className="settings-container">
                        <h2>Entity Details</h2>
                        {/*Action-button Starts*/}
                        <div className="hcm-action-btn">
                            <div className="action-btn">
                                <span className="hcmicon-action"></span>
                            </div>
                            <div className="hcm-action-elements">
                                <ul>
                                    <li><a href="#"><span className="hcmicon-save"></span></a></li>
                                    <li><a href="#"><span className="hcmicon-cancel"></span></a></li>
                                    <li><a href="#"><span className="hcmicon-edit"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        {/*Action-button Ends*/}
                        <div className="settings-sec">
                            <BootstrapTable selectRow={ selectRow } data={entityType} tableHeaderClass='react_table_head' striped={true} hover={true} >
                                <TableHeaderColumn dataField="sno" isKey={true} dataSort={true} width='62' dataAlign='center'>S.No</TableHeaderColumn>
                                <TableHeaderColumn dataField="name">Name</TableHeaderColumn>
                                <TableHeaderColumn dataField="displayname">Display Name</TableHeaderColumn>
                                <TableHeaderColumn dataField="status">Status</TableHeaderColumn>
                                <TableHeaderColumn dataField="createdby">Created By</TableHeaderColumn>
                                <TableHeaderColumn dataField="createddate">Created Date & Time</TableHeaderColumn>
                            </BootstrapTable>

                        </div>


                    </div>
                </div>

            </div>
        )
    }
};
