/**
 * Created by anakha on 6/19/17.
 */
import React,{Component} from 'react';
import Swiper from "swiper";
import "swiper/dist/css/swiper.css";
import "../stylesheets/css/forms.scss";
var FontAwesome = require('react-fontawesome');
var mySwiper;


export default class rmDetails extends Component{
    componentDidMount()
    {
        $('.floating-labe1 .form-control').on('focus blur', function (e) {
            $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');

        $('.floating-labe1 .form-control').focus(function () {
            $(this).data('placeholder', $(this).attr('placeholder'))
                .attr('placeholder', '');
        }).blur(function () {
            $(this).attr('placeholder', $(this).data('placeholder'));
        });

        //cloning div
        var regex = /^(.+?)(\d+)$/i;
        var cloneIndex = $(".repeatBox").length;

        function clone(){
            $(this).parents(".repeatBox").clone()
                .appendTo("#socialLinksLeft")
                .attr("id", "repeatBlock" +  cloneIndex)
                // .find("*")
                .each(function() {
                    var id = this.id || "";
                    var match = id.match(regex) || [];
                    if (match.length == 3) {
                        this.id = match[1] + (cloneIndex);
                    }
                })
                .on('click', '.clone', clone)
                .on('click', '.remove', remove);
            cloneIndex++;
        }
        function remove(){
            $(this).parents(".repeatBox").remove();
        }
        $(".clone").on("click", clone);
        $(".remove").on("click", remove);

        //swiper
        $(".tabs-circle").on("shown.bs.tab",function (){
            new Swiper('.swiper-container', {
                speed: 400,
                slidesPerView:'auto',
                spaceBetween:0,
            }).update(true);
        })

        /* $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
         $(this).find('.swiper-container')[0].swiper.update(true)
         });*/
    }
    render(){
        return (
            <div className="admin-padding-wrap">
                    <h2>Details</h2>

                    <div className="col-lg-12 col-md-12 col-sm-12 offset-0 ">
                        <ul className="nav nav-tabs tabs-circle col-lg-6 col-md-8 col-sm-10">
                            <li className="active"><a href="#info" data-toggle="tab"><span className="hcmicon-message"></span></a> <label>Info</label> </li>
                            <li><a href="#directors" data-toggle="tab"><span className="hcmicon-emp-team"></span></a> <label>Directors</label> </li>
                            <li><a href="#kycs" data-toggle="tab"><span className="hcmicon-certificate"></span></a> <label>KYCs</label> </li>
                            <li><a href="#socialLinks" data-toggle="tab"><span className="hcmicon-social-link"></span></a> <label>Social Links</label> </li>
                            <li><a href="#branches" data-toggle="tab"><span className="hcmicon-library"></span></a> <label>Branches</label> </li>
                        </ul>

                        <div className="tab-content rm-tab-container">
                            <div className="tab-pane active" id="info">
                                <div className="floating-labe1">
                                    <form>
                                        <div className="col-lg-6 col-md-6 col-sm-6">
                                            <div className="form-group">
                                                <label className="control-label">Group Name </label>
                                                <input type="text" className="form-control" placeholder="Group Name"/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Display Name </label>
                                                <input type="text" className="form-control" placeholder="Display Name"/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Registration No. </label>
                                                <input type="text" className="form-control" placeholder="Registration No."/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Registration Date </label>
                                                <input type="text" className="form-control" placeholder="Registration Date"/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Email ID </label>
                                                <input type="text" className="form-control" placeholder="Email ID"/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Phone No. </label>
                                                <input type="text" className="form-control" placeholder="Phone No."/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Business Type</label>
                                                <select className="form-control" placeholder="Business Type">
                                                    <option></option>
                                                    <option>Type 1</option>
                                                    <option>Type 2</option>
                                                </select>
                                            </div>

                                        </div>

                                        <div className="col-lg-6 col-md-6 col-sm-6">
                                            <div className="form-group">
                                                <label className="control-label">Upload Logo</label>
                                                <input type="text" className="form-control" placeholder="Certification Number"/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Certification Number</label>
                                                <input type="text" className="form-control" placeholder="Certification Number"/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Type of Group</label>
                                                <select className="form-control" placeholder="Type of Group">
                                                    <option></option>
                                                    <option>Type 1</option>
                                                    <option>Type 2</option>
                                                </select>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Domain</label>
                                                <select className="form-control" placeholder="Domain">
                                                    <option></option>
                                                    <option>Type 1</option>
                                                    <option>Type 2</option>
                                                </select>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Sub Domain</label>
                                                <select className="form-control" placeholder="Sub Domain">
                                                    <option></option>
                                                    <option>Type 1</option>
                                                    <option>Type 2</option>
                                                </select>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Group Head Name </label>
                                                <input type="text" className="form-control" placeholder="Email ID"/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Group Size </label>
                                                <input type="text" className="form-control" placeholder="Email ID"/>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label">Total Number of Managers </label>
                                                <input type="text" className="form-control" placeholder="Email ID"/>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div className="tab-pane" id="directors">
                                <div className="col-lg-4 col-md-4 col-sm-3 offset-0">
                                    <div className="user-card container-box ">
                                        <div className="user-pic"> <img src="/images/user-log.jpg"/> </div>
                                        <p> Pavan</p>
                                        <p> Group admin</p>
                                        <p> HCM -ID 123</p>
                                        <div className="container-row clearfix row-cols user-role user-role-br offset-0">
                                            <div className="col"> <p>Director</p> </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-3 offset-0">
                                    <div className="user-card container-box ">
                                        <div className="user-pic"> <img src="/images/user-log.jpg"/> </div>
                                        <p> Pavan</p>
                                        <p> Group admin</p>
                                        <p> HCM -ID 123</p>
                                        <div className="container-row clearfix row-cols user-role user-role-br offset-0">
                                            <div className="col"> <p>Director</p> </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 col-md-4 col-sm-3 offset-0">
                                    <div className="user-card container-box ">
                                        <div className="user-pic"> <img src="/images/user-log.jpg"/> </div>
                                        <p> Pavan</p>
                                        <p> Group admin</p>
                                        <p> HCM -ID 123</p>
                                        <div className="container-row clearfix row-cols user-role user-role-br offset-0">
                                            <div className="col"> <p>Director</p> </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="tab-pane" id="kycs">
                                <h2>Self</h2>
                                <div className="col-lg-4 col-md-4 col-sm-4">

                                    <div className="container-box">
                                        <div className="container-row">PAN Card</div>
                                        <div className="container-row clearfix swiper-container">
                                            <div className="row-cols card-proof-wrapp swiper-wrapper">
                                                <div className="card-proof col swiper-slide"><img src="/images/pan-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/pan-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/pan-card.jpg"/></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="container-box">
                                        <div className="container-row">Aadhar Card</div>
                                        <div className="container-row clearfix swiper-container">
                                            <div className="row-cols card-proof-wrapp swiper-wrapper">
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="container-box">
                                        <div className="container-row">Registration Document</div>
                                        <div className="container-row clearfix swiper-container">
                                            <div className="row-cols card-proof-wrapp swiper-wrapper">
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h2>Process</h2>
                                <div className="col-lg-4 col-md-4 col-sm-4">

                                    <div className="container-box">
                                        <div className="container-row">Certification Number</div>
                                        <div className="container-row clearfix swiper-container">
                                            <div className="row-cols card-proof-wrapp swiper-wrapper">
                                                <div className="card-proof col swiper-slide"><img src="/images/pan-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/pan-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/pan-card.jpg"/></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="container-box">
                                        <div className="container-row">Land Documents</div>
                                        <div className="container-row clearfix swiper-container">
                                            <div className="row-cols card-proof-wrapp swiper-wrapper">
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="container-box">
                                        <div className="container-row">Address Proof</div>
                                        <div className="container-row clearfix swiper-container">
                                            <div className="row-cols card-proof-wrapp swiper-wrapper">
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                                <div className="card-proof col swiper-slide"><img src="/images/Aadhar-card.jpg"/></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="tab-pane" id="socialLinks">
                                <div className="col-lg-6 col-md-6 col-sm-6" id="socialLinksLeft">
                                    <div className="col-lg-12 col-md-12 col-sm-12 repeatBox offset-0" id="repeatBlock">
                                        <form>
                                            <div className="floating-labe1">
                                                <div className="form-group">
                                                    <div className="rbox-head">Email ID 1
                                                        <div className="actions">
                                                            <div className="clone"><span className="hcmicon-add"></span></div>
                                                            <div className="remove"><span className="hcmicon-cancel"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">Email ID </label>
                                                    <input type="text" className="form-control" placeholder="Line1"/>
                                                </div>


                                                <div className="form-group">
                                                    <label className="control-label">Email ID </label>
                                                    <input type="text" className="form-control" placeholder="Line1"/>
                                                </div>

                                                <div className="form-group offset-0">
                                                    <label className="control-label">Email ID </label>
                                                    <input type="text" className="form-control" placeholder="Line1"/>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                                <div className="col-lg-6 col-md-6 col-sm-6" id="socialLinksRight">
                                    <div className="col-lg-12 col-md-12 col-sm-12 repeatBox offset-0" id="repeatBlock">
                                        <form>
                                            <div className="floating-labe1">
                                                <div className="form-group">
                                                    <div className="rbox-head">Social Links 1
                                                        <div className="actions">
                                                            <div className="clone"><span className="hcmicon-add"></span></div>
                                                            <div className="remove"><span className="hcmicon-cancel"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">Social Links </label>
                                                    <input type="text" className="form-control" placeholder="Line1"/>
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">Social Links </label>
                                                    <input type="text" className="form-control" placeholder="Line1"/>
                                                </div>
                                                <div className="form-group offset-0">
                                                    <label className="control-label">Social Links </label>
                                                    <input type="text" className="form-control" placeholder="Line1"/>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>

                            <div className="tab-pane" id="branches"> helo5

                            </div>
                        </div>

                    </div>

                </div>
        )
    }
};


