import { validateLoginCredentials } from './userObjectValidation';

module.exports = {
  validateLoginCredentials: validateLoginCredentials
}
