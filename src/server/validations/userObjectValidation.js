import joi from 'joi';

const loginCredentialSchema = joi.object().keys({
  email: joi.string().email().required(),
  password: joi.string().required()
});

function validateLoginCredentials(loginCredential) {
  const { error, value } = joi.validate(loginCredential, loginCredentialSchema);
  if(error){
    return({success:false, message: error.details[0].message});
  }else{
    return ({success:true});
  }
}

module.exports = {
  validateLoginCredentials: validateLoginCredentials
}
