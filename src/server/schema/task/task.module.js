const addTask = {
    queryName: 'addTask',
    moduleName: 'todo',
    operation: 'C',
    isDataVerificationReq: 'false'
};

const multipleTodos = {
    queryName: 'multipleTodos',
    moduleName: 'todo',
    operation: 'R',
    isDataVerificationReq: 'false'
};

const singleTodo = {
    queryName: 'singleTodo',
    moduleName: 'todo',
    operation: 'R',
    isDataVerificationReq: 'false'
};

const updateTask = {
    queryName: 'updateTask',
    moduleName: 'todo',
    operation: 'U',
    isDataVerificationReq: 'false'
};

const deleteTask = {
    queryName: 'deleteTask',
    moduleName: 'todo',
    operation: 'D',
    isDataVerificationReq: 'false'
};

module.exports = {
    addTask : addTask,
    multipleTodos : multipleTodos,
    singleTodo : singleTodo,
    updateTask : updateTask,
    deleteTask : deleteTask
}
